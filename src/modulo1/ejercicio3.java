package modulo1;

public class ejercicio3 {

	public static void main(String[] args) {
		System.out.println("Tecla de escape\t\tSignificado\n");
		System.out.println("\\n\t\t\tSignifica nueva l�nea");
		System.out.println("\\t\t\t\tSignifica un Tab de espacio");
		System.out.println("\\\"\t\t\tSe utiliza para escribir la \" (comilla doble) dentro del texto\n\t\t\tPor ejemplo: \"Belencita\"");
		System.out.println("\\\\\t\t\tSe utiliza para escribir la \\ dentro del texto \n\t\t\tPor ejemplo: \\algo\\");
		System.out.println("\\\'\t\t\tSe utiliza para escribir la \' (comilla simple) dentro del texto\n\t\t\tPor ejemplo: \'Princesita\'");
	}

}
