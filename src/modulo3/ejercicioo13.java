package modulo3;

import java.util.Scanner;

public class ejercicioo13 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Escriba el nombre del mes (con may�scula inicial) para averiguar la cantidad de dias que tiene: ");
		String mes = scanner.nextLine();
		
		switch(mes){
		case "Febrero":
			System.out.println(mes+" tiene 28 o 29 d�as.");
			break;
		case "Abril":
		case "Junio":
		case "Septiembre":
		case "Noviembre":
			System.out.println(mes+" tiene 30 d�as.");
			break;
		case "Enero":
		case "Marzo":
		case "Mayo":
		case "Julio":
		case "Agosto":
		case "Octubre":
		case "Diciembre":
			System.out.println(mes+" tiene 31 d�as.");
		}
		
		scanner.close();
	}
}
