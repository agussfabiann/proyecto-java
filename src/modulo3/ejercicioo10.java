package modulo3;

import java.util.Scanner;

public class ejercicioo10 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Ingrese el primer valor: ");
		int valor1 = scanner.nextInt();
		System.out.print("Ingrese el segundo valor: "); 
		int valor2 = scanner.nextInt();
		System.out.print("Ingrese el tercer valor: ");
		int valor3 = scanner.nextInt();
		
		if(valor2==valor3 && valor1==valor2) {System.out.println("Todos los valores son idénticos.");}
		else if(valor1>valor2 && valor2==valor3) {System.out.println("El primer valor es el mayor.");}
		else if(valor2>valor3 && valor1==valor3) {System.out.println("El segundo valor es el mayor.");}
		else if(valor3>valor1 && valor1==valor2) {System.out.println("El tercer valor es el mayor.");}
		else if(valor1>valor2 && valor2>valor3) {System.out.println("El primer valor es el mayor.");}
		else if(valor2>valor3 && valor1<valor2) {System.out.println("El segundo valor es el mayor.");}
		else if(valor2<valor3 && valor1<valor2) {System.out.println("El tercer valor es el mayor.");}
		else if(valor1>valor3 && valor1>valor3) {System.out.println("El primer y segundo valor son los mayores.");}
		else if(valor2==valor3 && valor2>valor1) {System.out.println("El primer y tercer valor son los mayores.");}
		else if(valor1==valor3 && valor1>valor2) {System.out.println("El segundo y tercer valor son los mayores.");}
		
		scanner.close();
	}
}
