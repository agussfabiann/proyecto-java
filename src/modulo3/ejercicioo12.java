package modulo3;

import java.util.Scanner;

public class ejercicioo12 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese un valor para determinar en que docena se encuentra (Rango de 1 a 36): "); 
		int valor = scanner.nextInt();
		
		if(valor>=1 && valor<=12) {System.out.println("El valor ingresado se encuentra en la primer docena.");}
		else if(valor>=13 && valor<=24) {System.out.println("El valor ingresado se encuentra en la segunda docena.");}
		else if(valor>=25 && valor<=36) {System.out.println("El valor ingresado se encuentra en la tercer docena.");}
		else if(valor<1 || valor>36) {System.out.println("Valor fuera de rango.");}
		
		scanner.close();
	}
}