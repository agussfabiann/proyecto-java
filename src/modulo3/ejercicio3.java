package modulo3;

import java.util.Scanner;

public class ejercicio3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Escriba el nombre del mes (con may�scula inicial) para averiguar la cantidad de dias que tiene: ");
		String mes = scanner.nextLine();
		
		if(mes.equals("Enero")){System.out.println(mes+" tiene 31 d�as.");}
		else if(mes.equals("Marzo")) {System.out.println(mes+" tiene 31 d�as.");}
		else if(mes.equals("Mayo")) {System.out.println(mes+" tiene 31 d�as.");}
		else if(mes.equals("Julio")) {System.out.println(mes+" tiene 31 d�as.");}
		else if(mes.equals("Agosto")) {System.out.println(mes+" tiene 31 d�as.");}
		else if(mes.equals("Octubre")) {System.out.println(mes+" tiene 31 d�as.");}
		else if(mes.equals("Diciembre")) {System.out.println(mes+" tiene 31 d�as.");}
		else if(mes.equals("Abril")) {System.out.println(mes+" tiene 30 d�as.");}
		else if(mes.equals("Junio")) {System.out.println(mes+" tiene 30 d�as.");}
		else if(mes.equals("Septiembre")) {System.out.println(mes+" tiene 30 d�as.");}
		else if(mes.equals("Noviembre")) {System.out.println(mes+" tiene 30 d�as.");}
		else if(mes.equals("Febrero")) {System.out.println(mes+" tiene 28 o 29 d�as.");}
		
		scanner.close();
	}

}
