package modulo3;

import java.util.Scanner;

public class ejercicio1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		double nota1, nota2, nota3, prom;
		
		System.out.print("Introduzca la nota 1  ");
		nota1 = scanner.nextDouble();
		System.out.print("Introduzca la nota 2  ");
		nota2 = scanner.nextDouble();
		System.out.print("Introduzca la nota 3  ");
		nota3 = scanner.nextDouble();
		
		prom = (nota1+nota2+nota3)/3;
		
		if(prom>=7){
			System.out.println("Esta aprobado, su promedio es de "+ prom);
		}
		else{
			System.out.println("Esta desaprobado, su promedio es de "+ prom);
		}
		
		scanner.close();
	}

}
