package modulo3;

import java.util.Scanner;

public class ejercicio4 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		String cat;
		
		System.out.print("Introduzca su categor�a (a, b o c):  ");
		cat = scanner.nextLine();
		
		if(cat.equals("a")) {System.out.println("La categor�a introducida corresponde a hijos.");}
		else if(cat.equals("b")) {System.out.println("La categor�a introducida corresponde a padres.");}
		else if(cat.equals("c")) {System.out.println("La categor�a introducida corresponde a abuelos.");}
		
		scanner.close();
	}

}
