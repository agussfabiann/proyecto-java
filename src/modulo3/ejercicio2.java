package modulo3;

import java.util.Scanner;

public class ejercicio2 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int n, r;
		
		System.out.print("Escriba un n�mero  ");
		n=scanner.nextInt();
		
		r=n%2;

		if(r==0){
			System.out.println("El n�mero escrito es par");
		}
		else{
			System.out.println("El n�mero escrito es impar");
		}
		
		scanner.close();
	}

}
