package modulo3;

import java.util.Scanner;

public class ejercicio8 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int p1, p2;
		
		System.out.println("Piedra: 0");
		System.out.println("Papel: 1");
		System.out.println("Tijera: 2");
		
		System.out.print("Jugador uno, haga su movimiento: ");
		p1 = scanner.nextInt();
		System.out.print("Jugador dos, haga su movimiento:  ");
		p2 = scanner.nextInt();
		
		if(p2==0) {
			if(p1==1) {System.out.println("Ganador: Jugador uno.");}
			else if(p1==2) {System.out.println("Ganador: Jugador dos.");}
			else {System.out.println("Empataron.");}
		}
		else if(p2==1) {
			if(p1==0) {System.out.println("Ganador: Jugador dos.");}
			else if(p1==2) {System.out.println("Ganador: Jugador uno.");}
			else {System.out.println("Empataron.");}
		}
		else if(p2==2) {
			if(p1==0) {System.out.println("Ganador: Jugador uno.");}
			else if(p1==1) {System.out.println("Ganador: Jugador dos.");}
			else {System.out.println("Empataron.");}
		}
		
		scanner.close();
	}

}

