package modulo3;

import java.util.Scanner;

public class ejercicioo15 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		char cat;
		System.out.println("Introduzca la categor�a a la que pertenece su autom�vil (a, b o c); ");

		cat = scanner.next().charAt(0);
		
		switch(cat){
		case 'a':
			System.out.println("Su autom�vil tiene: ");
			System.out.println("4 ruedas y un motor");
			break;
		case 'b':
			System.out.println("Su autom�vil tiene: ");
			System.out.println("4 ruedas, un motor, cierre centralizado y aire acondicionado.");
			break;
		case 'c':
			System.out.println("Su autom�vil tiene: ");
			System.out.println("4 ruedas, un motor, cierre centralizado, aire acondicionado y airbag.");
			break;
		}
		
		scanner.close();
	}

}