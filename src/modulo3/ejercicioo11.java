package modulo3;

import java.util.Scanner;

public class ejercicioo11 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Introduzca una letra: ");
		char let = scanner.next().charAt(0);
		
		if(let=='a'||let=='e'||let=='i'||let=='o'||let=='u'||let=='A'||let=='E'||let=='I'||let=='O'||let=='U') {System.out.println("La letra introducida es una vocal.");}
		else {System.out.println("La letra introducida no es una vocal, es una consonante.");}
		
		scanner.close();
	}

}