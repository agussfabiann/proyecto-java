package modulo3;

import java.util.Scanner;

public class ejercicio5 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Introduzca el n�mero del puesto que ha salido: ");
		String puesto = scanner.nextLine();
		
		if(puesto.equals("1")) {System.out.println("Ha salido primero, le corresponde la medalla de oro.");}
		else if(puesto.equals("2")) {System.out.println("Ha salido segundo, le corresponde la medalla de plata.");}
		else if(puesto.equals("3")) {System.out.println("Ha salido tercero, le corresponde la medalla de bronce.");}
		else {System.out.println("Usted ha salido en el puesto n�" +puesto+ ", mas suerte en la pr�xima ocasi�n.");}
		
		scanner.close();
	}

}
