package modulo3;

import java.util.Scanner;

public class ejercicioo17 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese un valor para averiguar sus 10 primeros m�ltiplos y la suma de los valores pares: ");
		int valor = scanner.nextInt();
		int a=1;
		int suma = 0;
		int rta;
		
		while (a<11) {
			System.out.println(valor+"x"+a+"="+(valor*a));
			rta=valor*a;
			if (rta%2==0) {suma=suma+valor*a;}
			a++;
		}
		System.out.println("Los valores pares sumados dan un resultado de:  "+suma);
		
		scanner.close();
	}
}

