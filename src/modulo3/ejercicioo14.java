package modulo3;

import java.util.Scanner;

public class ejercicioo14 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Introduzca el n�mero del puesto que ha salido: ");
		String puesto = scanner.nextLine();
		
		switch(puesto){
		case "1":
			System.out.println("Ha salido primero, le corresponde la medalla de oro.");
			break;
		case "2":
			System.out.println("Ha salido segundo, le corresponde la medalla de plata.");
			break;
		case "3":
			System.out.println("Ha salido tercero, le corresponde la medalla de bronce.");
			break;
		default:
			System.out.println("Usted ha salido en el puesto n�" + puesto + ", mas suerte en la pr�xima ocasi�n.");
		}
		
		scanner.close();
	}
}
