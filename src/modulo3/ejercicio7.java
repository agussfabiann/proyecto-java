package modulo3;

import java.util.Scanner;

public class ejercicio7 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int valor1, valor2, valor3;
		
		System.out.print("Ingrese el primer valor: ");
		valor1 = scanner.nextInt();
		System.out.print("Ingrese el segundo valor: ");
		valor2 = scanner.nextInt();
		System.out.print("Ingrese el tercer valor: ");
		valor3 = scanner.nextInt();
		
		if(valor1==valor2){
			if(valor3>valor1) {System.out.println("El tercer valor es el mayor.");}
			else if(valor3<valor1){System.out.println("El primer y segundo valor son los mayores.");}
			else {System.out.println("Todos los valores son idénticos.");}
		}
		else if(valor2==valor3){
			if(valor1>valor2) {System.out.println("El primer valor es el mayor.");}
			else if(valor1<valor2) {System.out.println("El segundo y tercer valor son los mayores.");}
			else {System.out.println("Todos los valores son idénticos.");}
		}
		else if(valor1==valor3){
			if(valor2>valor1) {System.out.println("El segundo valor es el mayor.");}
			else if(valor2<valor1) {System.out.println("El primer y tercer valor son los mayores.");}
			else {System.out.println("Todos los valores son idénticos.");}
		}
		else if(valor1>valor2){
			if(valor2>valor3) {System.out.println("El primer valor es el mayor.");}
			else if(valor3>valor2){
				if(valor3>valor1) {System.out.println("El tercer valor es el mayor.");}
				else {System.out.println("El primer valor es el mayor.");}
			}
		}
		else if(valor1>valor3){
			if(valor3>valor2) {System.out.println("El primer valor es el mayor.");}
			else if(valor2>valor3){
				if(valor2>valor1) {System.out.println("El segundo valor es el mayor");}
				else {System.out.println("El primer valor es el mayor.");}
			}
		}
		else if(valor2>valor1){
			if(valor1>valor3) {System.out.println("El segundo valor es el mayor");}
			else if(valor3>valor1){
				if(valor3>valor2) {System.out.println("El tercer valor es el mayor.");}
				else {System.out.println("El segundo valor es el mayor");}
			}
		}
		else if(valor3>valor2){
			if(valor2>valor1) {System.out.println("El tercer valor es el mayor.");}
			else if(valor1>valor2){
				if(valor1>valor3) {System.out.println("El primer valor es el mayor.");}
				else {System.out.println("El tercer valor es el mayor.");}
			}
		}
		
		scanner.close();
	}
}