package modulo3;

import java.util.Scanner;

public class ejercicio9 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Piedra: 0");
		System.out.println("Papel: 1");
		System.out.println("Tijera: 2");
		
		System.out.print("Jugador uno, haga su movimiento: ");
		int p1 = scanner.nextInt();
		System.out.print("Jugador dos, haga su movimiento: ");
		int p2 = scanner.nextInt();
		
		if(p1==0 && p2==0) {System.out.println("Empataron.");}
		else if(p1==1 && p2==1) {System.out.println("Empataron.");}
		else if(p1==2 && p2==2) {System.out.println("Empataron.");}
		else if(p1==2 && p2==1) {System.out.println("Ganador: Jugador uno.");}
		else if(p1==0 && p2==2) {System.out.println("Ganador: Jugador uno.");}
		else if(p1==1 && p2==0) {System.out.println("Ganador: Jugador uno.");}
		else if(p1==1 && p2==2) {System.out.println("Ganador: Jugador dos.");}
		else if(p1==2 && p2==0) {System.out.println("Ganador: Jugador dos.");}
		else if(p1==0 && p2==1) {System.out.println("Ganador: Jugador dos.");}
		
		scanner.close();
	}

}
