package modelo.componentes.test;

import static org.junit.Assert.*;

import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.AssertionFailedError;
import modelo.componentes.Capacitor;
import modelo.componentes.Componente;
import modelo.componentes.Inductor;
import modelo.componentes.Resistencia;

public class ComponenteTest {
	
	//defino los objetos que voy a utlizar para esl testeo
	//lote de pruebass
	Inductor inductorVacio ; //1- definiccion
	Inductor inductorConParametros ; //1- definiccion
	
	Capacitor capacitorVacio;
	Capacitor capacitorConParametros;
	
	Resistencia resistenciaVacia;
	Resistencia resistenciaConParametros;
	
	
	List<Componente> componentesList;
	Set<Componente>  componentesSet;
	@Before
	public void setUp() throws Exception {
		inductorVacio 			 = new Inductor();
		inductorConParametros 	 = new Inductor("L1", 0.003f, 6000);
		
		capacitorVacio 			 = new Capacitor();
		capacitorConParametros 	 = new Capacitor("C1", 0.000001f,  1000);
		
		resistenciaVacia 		 = new Resistencia();
		resistenciaConParametros = new Resistencia("R1", 100);
		
		//creo mi lote de pruebas con una List
		componentesList = new ArrayList<>(); //ordered
		componentesList.add(new Inductor("indu1",0.1f, 100));
		componentesList.add(new Inductor("indu2",0.2f, 200));
		componentesList.add(new Inductor("indu3",0.3f, 300));
		componentesList.add(new Inductor("indu4",0.4f, 400));
		componentesList.add(new Inductor("indu5",0.5f, 500));
		
		componentesList.add(new Capacitor("cap1", 0.001f, 100));
		componentesList.add(new Capacitor("cap2", 0.002f, 200));
		componentesList.add(new Capacitor("cap3", 0.003f, 300));
		componentesList.add(new Capacitor("cap4", 0.004f, 400));
		componentesList.add(new Capacitor());
		
		componentesList.add(new Resistencia("Res1",100));
		componentesList.add(new Resistencia("Res2",200));
		componentesList.add(new Resistencia("Res3",300));
		componentesList.add(new Resistencia("Res4",400));
		
		//cre mi lote de pruebas con una Set
		componentesSet = new HashSet<>(); //ordered
		componentesSet.add(new Inductor("indu1",0.1f, 100));
		componentesSet.add(new Inductor("indu2",0.2f, 200));
		componentesSet.add(new Inductor("indu3",0.3f, 300));
		componentesSet.add(new Inductor("indu4",0.4f, 400));
		componentesSet.add(new Inductor("indu5",0.5f, 500));

		
		componentesSet.add(new Capacitor("cap1", 0.001f, 100));
		componentesSet.add(new Capacitor("cap2", 0.002f, 200));
		componentesSet.add(new Capacitor("cap3", 0.003f, 300));
		componentesSet.add(new Capacitor("cap4", 0.004f, 400));
		componentesSet.add(new Capacitor());
		
		componentesSet.add(new Resistencia("Res1",100));
		componentesSet.add(new Resistencia("Res2",200));
		componentesSet.add(new Resistencia("Res3",300));
		componentesSet.add(new Resistencia("Res4",400));

		
	}

	@After
	public void tearDown() throws Exception {
		
		//lote de pruebas
		inductorVacio 			= null;
		inductorConParametros 	= null;
		
		capacitorVacio 			= null;
		capacitorConParametros  = null;
		
		resistenciaVacia 		= null;
		resistenciaConParametros= null;
						
		componentesList			= null;
		componentesSet 			= null;
	}

	@Test
	public void testResistor_Set_add_TRUE_noDeberia(){
		//TODO alumnos agregar el codigo, pueden modificar el lote de pruebas de creelo necesario
		Resistencia Res1 = new Resistencia("Res1",100);
		assertFalse(componentesSet.add(Res1));
	}
	@Test
	public void testResistor_Set_add_FALSE_siDeberiaAgregarse(){
		//TODO alumnos agregar el codigo, pueden modificar el lote de pruebas de creelo necesario
		Resistencia Res5 = new Resistencia("Res5",500);
		assertTrue(componentesSet.add(Res5));
	}

	@Test
	public void testCapacitor_Set_add_TRUE(){
		Capacitor otro = new Capacitor();
		otro.setNombre("gabrielito");		
		assertTrue(componentesSet.add(otro));
	}
	
	@Test
	public void testResistor_Set_add_FALSE(){
		//TODO alumnos agregar el codigo, pueden modificar el lote de pruebas de creelo necesario
		Resistencia Res1 = new Resistencia("Res1",100);
		assertFalse(componentesSet.add(Res1));
	}

	@Test
	public void testCapacitor_Set_add_FALSE(){
		Capacitor otro = new Capacitor();				
		assertFalse(componentesSet.add(otro));
	}
	@Test
	public void testCapacitor_list_contains_Equals_TRUE(){
		Capacitor otro = new Capacitor();
		assertTrue(componentesList.contains(otro));
		
	}
	@Test
	public void testCapacitor_list_contains_Equals_FALSE(){
		Capacitor otro = new Capacitor();
		otro.setNombre("Gabrielito");
		assertFalse(componentesList.contains(otro));
		
	}

	@Test
	public void testCapaciorVacioNombre(){
		assertEquals("capDef", capacitorVacio.getNombre());
	}
// ******** Contro del constructor vacio
	@Test
	public void testInductorVacioNombre() {
		assertEquals("Ldef", inductorVacio.getNombre());
		
	}
	@Test
	public void testInductorVacioUnidad() {
		assertEquals("Hy", inductorVacio.getUnidad());		
	}
	
	@Test
	public void testCalcularImpedanciaInductorVacio() {
		assertEquals(6.28, inductorVacio.calcularImpedancia(), 0.01);		
	}
	//de estos 25 como  minimo
//*************fin de test de constructor Vacio ******
	//TODO Alumnos hay que agregar el tes teo de lequals con prametros
	
	@Test
	public void testInductorConParametrosEqualsNombre() {
		assertEquals("L1", inductorConParametros.getNombre());
	}
	@Test
	public void testInductorConParametrosEqualsValor() {
		assertEquals(0.003f, inductorConParametros.getValor(), 0.0001f);
	}  //Con el ltimo valor podemos designar la tolerancia que hay debido a que el float en equals no se redondea
	@Test
	public void testInductorConParametrosEqualsUnidad() {
		assertEquals("Hy", inductorConParametros.getUnidad());
	}
	
	@Test
	public void testInductorVacioEquals() {
		Inductor otro = new Inductor();		
		assertTrue(inductorVacio.equals(otro));		
	}
	@Test
	public void testInductorConParamerieEqualsTRUE() {
		//TODO agregar el codigo 
		Inductor L1 = new Inductor("L1", 0.003f,6000);
		assertTrue(inductorConParametros.equals(L1));
	}
	@Test
	public void testInductorConParamerieEqualsFALSE() {
		//TODO agregar el codigo 
		Inductor L1 = new Inductor("L2",0.003f,6000);
		assertFalse(inductorConParametros.equals(L1));
	}

	@Test
	public void testEqalsEmListTRUE(){
		Inductor otro = new Inductor("indu2",0.2f, 200);
		assertTrue(componentesList.contains(otro));
	}
	@Test
	public void testEqualsEnListFALSE(){
		assertFalse(componentesList.contains(inductorVacio));
	}
	@Test
	public void testEqualsSetTrue(){
		
		assertFalse( componentesSet.add(new Inductor("indu3",0.3f, 300)));
		
	}

}
