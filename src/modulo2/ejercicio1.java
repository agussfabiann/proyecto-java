package modulo2;

public class ejercicio1 {

	public static void main(String[] args) {
		byte b = 127;
		int i = 34000;
		long l = 23424;
		
		long suma = b+i+l;
		float division = (float)i/b;
		
		byte OtroByte = (byte)i;
		
		char letra = 'a';
		int resto = b%2; 
		
		System.out.println("b = " + b);
		System.out.println("i = " + i);
		System.out.println("l = " + l);
		System.out.println("OtroByte " + OtroByte);
		System.out.println("el resto de " + b + " dividido por 2 da " + resto);
		
		System.out.println("suma = "+ suma);
		System.out.println("division = " + division);
	}

}
