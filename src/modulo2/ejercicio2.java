package modulo2;

public class ejercicio2 {

	public static void main(String[] args) {
		
		byte 		bmin = -128;
		byte 		bmax = 127;
		short 		smin = -32768;
		short 		smax = 32767;
		int 		imin = -2147483648;
		int 		imax = 2147483647;
		long 		lmin1 = -922337203;
		long 		lmin2 = 685477580;
		long 		lmin3 = 8;
		long 		lmax1 = 922337203;
		long		lmax2 = 68547758;
		long		lmax3 = 07;
		
		System.out.println("tipo\tminimo\t\t\tmaximo");
		System.out.println("....\t......\t\t\t......");
		System.out.println("\nbyte\t" + bmin + "\t\t\t" + bmax);
		System.out.println("\nshort\t" + smin + "\t\t\t" + smax);
		System.out.println("\nint\t" + imin + "\t\t" + imax);
		System.out.println("\nlong\t" + lmin1 + lmin2 + lmin3 + "\t" + lmax1 + lmax2 + lmax3);

		/*�Cu�l es la f�rmula general que me permite mostrar los m�nimos y los m�ximos teniendo en cuenta la cantidad de bits?
		La f�rmula general que me permite mostrar los m�nimos y los m�ximos teniendo en cuenta la cantidad de bits es: "2^n-1"
		*/
	}

}
