package modulo2;

public class ejercicio3 {

	public static void main(String[] args) {
		//ALOJAR EN VARIABLES QUE OCUPEN LA MENOR MEMORIA POSIBLE
		
		//Tipo de divisi�n a, b o c
		char letra = 'b';
		
		//Cantidad de goles por partido [Por ej: 8 como en Brasil vs Alemania en 2014 (1-7)]
		byte GxP = 8;
		
		//Capacidad de un estadio [Por ej: 114000 el Estadio Reungrado Primero de Mayo (El de mayor capacidad)]
		int CapEst = 114000;
		
		//Promedio de goles [Por ej: 1,052 como Palermo en el apertura del '98 (20 goles en 19 partidos)]
		float PromGoles = 1.052f;
		
		System.out.println("Letra " + letra);
		System.out.println("La cantidad de goles en el partido fue de " + GxP + " tantos");
		System.out.println("La capacidad del estadio es de " + CapEst + " personas");
		System.out.println("El promedio de goles fue de " + PromGoles);
	}

}
